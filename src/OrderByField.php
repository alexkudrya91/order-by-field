<?php

namespace AlexKudrya\OrderByField;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use PDO;

trait OrderByField
{
    /**
     * @author Kudria Alexander <alexkudrya91@gmail.com>
     * @description Usage: ...$query->orderByField([field name], [array of values])
     *
     * @param Builder $builder
     * @param string $fieldName
     * @param array $values
     * @return Builder
     */
    public function scopeOrderByField(Builder $builder, string $fieldName, array $values): Builder
    {
        if (empty($values)) {
            return $builder;
        }

        $driver = DB::connection()->getPDO()->getAttribute(PDO::ATTR_DRIVER_NAME);

        if ($driver === 'pgsql') {
            $raw = 'CASE ';

            foreach ($values as $key => $value) {
                $raw .= "WHEN ($fieldName = $value) THEN $key ";

                if ($key == (count($values) - 1)) {
                    $raw .= "ELSE " . $key+1 . " END";
                }
            }

            $builder->orderByRaw($raw);
        }

        if ($driver === 'mysql') {
            $raw = 'FIELD(' . $fieldName;

            foreach ($values as $key => $value) {
                $raw .= ", \"$value\"";
            }

            $raw .= ")";

            $builder->orderByRaw($raw);
        }

        return $builder;
    }
}