# Sql ORDER BY FILED() function for Laravel
It is trait with `scope` function for Laravel applications

## Installation

```shell
composer require alex-kudrya/orderbyfield
```

## Usage


Example:
```php
// app/Models/User.php

namespace App\Models;

use AlexKudrya\OrderByField\OrderByField;

class User extends Model
{
    use OrderByField;

    ...
}

```

Now you can use `orderByField()` method:

```php

$ordered_list = User::orderByField('role_id', [4,2])->get();
```
Now in `$ordered_list` all users sorted in same order: first users with **role_id = 4**, second **role_id = 2**, and than rest of them.
